=====================
Knot Resolver Manager
=====================

.. toctree::
    :maxdepth: 2
    :caption: Manager:

    manager-introduction
    manager-getting-started
    manager-migration
    manager-configuration

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
